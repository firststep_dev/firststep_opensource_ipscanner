@echo off
echo Cleaning up old build files...
if exist build rmdir /s /q build
if exist dist rmdir /s /q dist
echo Running PyInstaller...
pyinstaller main.spec
pause
