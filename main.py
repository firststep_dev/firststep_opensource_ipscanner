import os
import subprocess
import sys
import socket
import threading
import tkinter as tk
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor, as_completed
from scapy.all import ARP, Ether, srp
import time
from PIL import Image, ImageTk
import signal

VERSION="2024.01.18.004"

import sys
import logging
logging.basicConfig(filename='log.log', level=logging.DEBUG, format='%(asctime)s [%(levelname)s]: %(message)s')
sys.stdout = open('log.log', 'a')
sys.stderr = sys.stdout

# Install scapy if not already installed
def install_package(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

try:
    import manuf_local as manuf
    print(manuf.__file__)
except ModuleNotFoundError:
    root = tk.Tk()
    root.withdraw()  # Hide the main window
    messagebox.showerror("Python Module Not Found", "MANUF module not found. Please contact support.")
    root.destroy()
    sys.exit(1)

try:
    from scapy.all import conf
    if not conf.use_pcap:
        raise ImportError
except ImportError:
    root = tk.Tk()
    root.withdraw()  # Hide the main window
    messagebox.showerror("Npcap Not Found", "Npcap not found. Please download and install Npcap from: https://nmap.org/npcap/")
    root.destroy()
    sys.exit(1)

# Function to extract IP addresses from 'ipconfig' command output
def get_ip_addresses():
    try:
        result = subprocess.run(['ipconfig'], capture_output=True, text=True, check=True)
        output = result.stdout
        # Find lines containing IPv4 addresses
        ipv4_lines = [line.strip() for line in output.splitlines() if 'IPv4 Address' in line]
        # Extract the IP addresses
        ip_addresses = [line.split(': ')[-1] for line in ipv4_lines]

        # Modify the last item of each IP address to 'x'
        modified_ip_addresses = [ip.rsplit('.', 1)[0] + '.x' for ip in ip_addresses]

        return modified_ip_addresses
    except subprocess.CalledProcessError as e:
        return []

# Function to look up manufacturer from MAC address
def get_manufacturer(mac):
    try:
        p = manuf.MacParser()
        return p.get_manuf(mac)
    except FileNotFoundError:
        logging.error("Manufacturer data file not found.")
        return "Unknown"

# Scan a single IP
def scan_ip(ip):
    arp_request = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=ip)
    answered, _ = srp(arp_request, timeout=1, verbose=False)

    for _, received in answered:
        if received.haslayer(ARP):
            if received[ARP].op == 2:  # ARP reply
                mac_address = received[ARP].hwsrc
                manufacturer = get_manufacturer(mac_address)
                time_found = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                return ip, f'{manufacturer}', time_found
    return None

def ip_key(ip):
    return tuple(map(int, ip.split('.')))

# Scan IP range in parallel and update GUI table
def scan_ip_range(ip_var, table, log_text):

    while True:

        ip_base = ip_var.get()[:-2]  # Get the selected IP address/range
        add_log("Scanning IP using base: %s" % ip_base, log_text)

        scan_results = []

        with ThreadPoolExecutor(max_workers=255) as executor:
            futures = [executor.submit(scan_ip, "%s.%d" % (ip_base, i)) for i in range(1, 256)]
            for future in as_completed(futures):
                result = future.result()
                if result:
                    scan_results.append(result)

        # Sort results numerically by IP address
        scan_results.sort(key=lambda x: ip_key(x[0]))

        # Update GUI table
        for i in table.get_children():
            table.delete(i)

        for ip, comment, time_found in scan_results:
            table.insert('', 'end', values=(ip, comment, time_found))

        add_log("Scan completed. %d IP addresses found." % (len(scan_results)), log_text)
        time.sleep(0.1)  # Wait for 0.1 seconds before the next scan

# Handle Ctrl+C
def signal_handler(sig, frame):
    root.destroy()

# Create a function to add logs
def add_log(message, log_text):
    log_message = "[%s] %s" % (datetime.now(), message)
    print(log_message)
    logs.append(log_message)
    log_text.config(state=tk.NORMAL)
    log_text.insert(tk.END, log_message + '\n')
    log_text.config(state=tk.DISABLED)

# Main function
def main():
    try:
        # Get a list of available IP addresses
        available_ip_addresses = get_ip_addresses()

        # Setup GUI
        global root
        root = tk.Tk()
        root.title("FirstStep.ai IP Scanner (v%s)" % VERSION)

        # Create a notebook (tabbed interface)
        notebook = ttk.Notebook(root)
        notebook.pack(fill=tk.BOTH, expand=True)

        # Main Application Tab
        main_frame = ttk.Frame(notebook)
        notebook.add(main_frame, text="Scanning...")

        # Log Tab
        log_frame = ttk.Frame(notebook)
        notebook.add(log_frame, text="Log")

        # Create a Text widget to display logs
        log_text = ScrolledText(log_frame, wrap=tk.WORD, state=tk.DISABLED)
        log_text.pack(fill=tk.BOTH, expand=True)

        # Example: Adding a log
        add_log("Initializing...", log_text)

        # Example: Showing logs in the log tab
        # show_logs()

        # Load and set the icon
        if getattr(sys, 'frozen', False):
            # If the application is run as a bundle, the pyInstaller bootloader
            # extends the sys module by a flag frozen=True and sets the app
            # path into variable _MEIPASS'.
            application_path = sys._MEIPASS
        else:
            application_path = os.path.dirname(os.path.abspath(__file__))

        icon_path = os.path.join(application_path, 'icon.ico')
        root.iconbitmap(default=icon_path)

        # Create a drop-down for selecting IP addresses
        ip_label = ttk.Label(main_frame, text="Select IP Address/Range:")
        ip_label.pack(pady=10)

        ip_var = tk.StringVar(root)
        ip_var.set(available_ip_addresses[0])  # Set the default value to the first available IP address

        ip_dropdown = ttk.Combobox(main_frame, textvariable=ip_var, values=available_ip_addresses)
        ip_dropdown.pack(pady=5)

        table = ttk.Treeview(main_frame, columns=('IP', 'Comment', 'Time Found'), show='headings')
        table.heading('IP', text='IP Address', anchor='w')
        table.heading('Comment', text='Device Manufacturer', anchor='w')
        table.heading('Time Found', text='Last seen', anchor='w')
        table.column('IP', anchor='w')
        table.column('Comment', anchor='w')
        table.column('Time Found', anchor='w')
        table.pack(expand=True, fill='both')

        # Handle Ctrl+C
        signal.signal(signal.SIGINT, signal_handler)

        # Start scanning in a separate thread
        scanning_thread = threading.Thread(target=scan_ip_range, args=(ip_var, table, log_text))
        scanning_thread.daemon = True
        scanning_thread.start()

        root.mainloop()
    except Exception as e:
        # Log the exception to the GUI log tab
        print(f"An error occurred: {str(e)}", log_text)
        add_log(f"An error occurred: {str(e)}", log_text)

if __name__ == "__main__":
    logs = []  # Global list to store logs
    main()
